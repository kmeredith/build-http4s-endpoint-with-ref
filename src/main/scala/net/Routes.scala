package net

import cats.effect._
import org.http4s._
import org.http4s.dsl.io._

import java.util.concurrent.atomic.AtomicReference

object Routes {

  // volatile
  // synchronized - multiple instructions
  // AtomicReference - 1 instruction
  private var counter: Int = 0

  // M[A] for Monad M
  // flatMap: A => M[B]
  // Comonad: G[B] => A

  //AtomicReference
  // case class wrapping AR
  def route(ref: Ref[IO, Int]): HttpApp[IO] = HttpApp[IO] {
    case GET -> Root / "hits" =>
      ref.get.map { i: Int =>
        Response[IO](status = Status.Ok).withEntity[String](i.toString)
      }
    case POST -> Root / "hits" =>
      ref.update(_ + 1).as(Response[IO](status = Status.NoContent))
  }

}
