package net

import cats.effect._
import cats.implicits._
import org.http4s.blaze.server.BlazeServerBuilder

object Main extends IOApp {

  def run(args: List[String]): IO[ExitCode] =
    Ref.of[IO, Int](0).flatMap { ref: Ref[IO, Int] =>
      BlazeServerBuilder[IO]
        .bindHttp(8080, "localhost")
        .withHttpApp(Routes.route(ref))
        .serve
        .compile
        .drain
        .as(ExitCode.Success)
    }


}