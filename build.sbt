scalaVersion := "2.12.13"

libraryDependencies ++= Seq(
  "org.http4s" %% "http4s-core" % "0.23.7",
  "org.http4s" %% "http4s-blaze-server" % "0.23.7",
  "org.http4s" %% "http4s-dsl" % "0.23.7"
)

scalacOptions ++= Seq("-Ypartial-unification")
